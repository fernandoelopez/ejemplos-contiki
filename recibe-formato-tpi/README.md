Este ejemplo está compuesto de 2 programas individuales compilados y una simulación de Cooja.

- `envia.z1`: Envía mensajes por UDP cada 5 segundos, algunos son válidos y
  otros no.
- `recibe.z1`: Recibe esos mensajes imprimiendo con `printf` los detalles de la
  información recibida.
- `simulacion.csc`: Es una simulación de Cooja que tiene 2 motas:
    1. Ejecuta `envia.z1`.
    2. Ejecuta `recibe.z1`.

Estos ejemplos (principalmente `recibe.z1`) pueden usarse para verificar si su
implementación del trabajo envía los datos de forma correcta.

`recibe.z1` no guarda los últimos 20 mensajes, solo imprime si recibe un
mensaje de repetición sin verificar si el número de secuencia pedido es
válido.
