/*
 * Copyright (c) 2011, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-debug.h"

#include "sys/node-id.h"

#include "simple-udp.h"

#include <stdio.h>
#include <string.h>

// Puerto UDP a usar (en este caso se usa el mismo puerto para enviar y recibir pero pueden ser distintos).
#define UDP_PORT 1234

// Tiempo entre mensajes (se usa para establecer un timer)
#define SEND_INTERVAL		(2 * CLOCK_SECOND)  // 2 * CLOCK_SECOND son 2 segundos.

// Variable que representa la conexión.
static struct simple_udp_connection unicast_connection;

/*---------------------------------------------------------------------------*/
PROCESS(unicast_sender_process, "Unicast sender example process");
AUTOSTART_PROCESSES(&unicast_sender_process);
/*---------------------------------------------------------------------------*/
static void receiver(struct simple_udp_connection *c,
    const uip_ipaddr_t *sender_addr,
    uint16_t sender_port,
    const uip_ipaddr_t *receiver_addr,
    uint16_t receiver_port,
    const uint8_t *data,
    uint16_t datalen)
{
    /**
     * Esta función se invoca por cada mensaje recibido. `enviar.c` no la usa,
     * pero se puede declarar igual.
     */
    printf("Data received on port %d from port %d with length %d\n",
            receiver_port, sender_port, datalen);
}

PROCESS_THREAD(unicast_sender_process, ev, data)
{
    // Timer para definir el intervalo entre los mensajes.
    static struct etimer periodic_timer;
    // Dirección IP destino.
    static uip_ipaddr_t addr;

    PROCESS_BEGIN();

    uip_ip6addr(&addr, 0xfe80, 0, 0, 0, 0xc30c, 0, 0, 2);

    // Inicializa la conexión
    simple_udp_register(&unicast_connection,
                        UDP_PORT,           // Puerto local
                        &addr,              // Dirección IP destino
                        UDP_PORT,           // Puerto destino
                        receiver);          // Función a invocar si recibo datos
                                            // en ese caso el último argumento
                                            // podría ser NULL porque no voy
                                            // a recibir nada


    // Establezco el timer para que genere un evento en la cantidad de tiempo
    // indicada por SEND_INTERVAL
    etimer_set(&periodic_timer, SEND_INTERVAL);

    while(1) {

        // Espero a que el timer expire y genere un evento.
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&periodic_timer));

        // Vuelvo a establecer el timer para que gener el próximo evento.
        etimer_reset(&periodic_timer);

        static unsigned int message_number;
        static char buf[20];

        printf("Sending unicast to ");
        // Imprime en pantalla la IP del destino.
        uip_debug_ipaddr_print(&addr);
        printf("\n");
        // Armamos el mensaje en buf
        sprintf(buf, "Message %d", message_number);
        message_number++;

        // Envíamos el mensaje
        simple_udp_send(&unicast_connection,    // Conexión
                        buf,                    // Puntero a los datos
                        strlen(buf) + 1);       // Cantidad de bytes a enviar
    }

    PROCESS_END();
}
/*---------------------------------------------------------------------------*/
