Este ejemplo está compuesto de 2 programas individuales y una simulación de Cooja.

- `envia.c`: Envía mensajes por UDP cada 2 segundos.
- `recibe.c`: Recibe esos mensajes.
- `simulacion.csc`: Es una simulación de Cooja que tiene 2 motas:
    1. Ejecuta `envia.z1`.
    2. Ejecuta `recibe.z1`.

Estos ejemplos muestran el uso de UDP para comunicaciones y de timers para
demorar la ejecución del proceso entre mensaje y mensaje en el caso de
`envia.c`.
